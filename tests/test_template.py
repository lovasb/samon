from tests.base import SamonTestCase
from tests.mock import get_base_environment


class TestTemplate(SamonTestCase):
    def test_parse_serialize(self):
        env = get_base_environment()
        template = env.get_template('template2.xml')
        self.assertXmlEqual(template.serialize(output='xml'), 'assets/base/template2.xml')
