from io import StringIO

from samon import constants, registry
from samon.elements import BaseElement
from samon.render import RenderedElement
from tests.base import SamonTestCase
from tests.mock import get_base_environment


class TestRenderedElement(SamonTestCase):
    def test_base(self):
        xml_attrs = {  # AttributesNSImpl like object
            (None, 'attr1'): 'val1',  # NS, attr_name
        }
        belement = BaseElement(xml_tag='base-element', xml_attrs=xml_attrs)
        context = {}
        relement = belement.render(context=context)

        self.assertIsInstance(relement, RenderedElement)
        self.assertTrue(hasattr(relement, 'children'))
        self.assertEqual(relement.node_name, belement.xml_tag)
        self.assertEqual(relement.node_attributes, {'attr1': 'val1'})
        self.assertEqual(relement._context, context)
        self.assertEqual(relement._element, belement)

    def test_data_binding(self):
        xml_attrs = {
            (constants.XML_NAMESPACE_DATA_BINDING, 'attr2'): 'val'
        }

        e = BaseElement(xml_tag='tag', xml_attrs=xml_attrs)
        xml = e.render(context={'val': 'example_value'}).serialize(output='xml')
        self.assertEqual(xml, '<tag attr2="example_value">\n</tag>\n')

    def test_eval_forloop(self):
        xml_attrs = {
            (constants.XML_NAMESPACE_FLOW_CONTROL, 'for'): 'a in val',
            (None, 'class'): 'class_name'
        }

        root = BaseElement(xml_tag='root', xml_attrs={})
        e = BaseElement(xml_tag='tag', xml_attrs=xml_attrs)
        root.children.append(e)
        xml = root.render(context={'val': [1, 2, 3]}).serialize(output='xml')
        self.assertXmlEqual(xml, 'assets/elements/forloop.xml')

    def test_eval_if(self):
        xml_attrs = {
            (constants.XML_NAMESPACE_FLOW_CONTROL, 'if'): 'val == 7',
            (None, 'class'): 'class_name'
        }

        root = BaseElement(xml_tag='root', xml_attrs={})
        e = BaseElement(xml_tag='tag', xml_attrs=xml_attrs)
        root.children.append(e)
        xml = root.render(context={'val': 8}).serialize(output='xml')
        self.assertEqual(xml, '<root>\n</root>\n')

        xml = root.render(context={'val': 7}).serialize(output='xml')
        self.assertXmlEqual(xml, 'assets/elements/ifcond.xml')

    def test_if_and_for_precedence(self):
        xml_attrs = {
            (constants.XML_NAMESPACE_FLOW_CONTROL, 'if'): 'val > 7',
            (constants.XML_NAMESPACE_FLOW_CONTROL, 'for'): 'val in val2',
            (constants.XML_NAMESPACE_DATA_BINDING, 'attr1'): 'val',
        }

        root = BaseElement(xml_tag='root', xml_attrs={})
        e = BaseElement(xml_tag='tag', xml_attrs=xml_attrs)
        root.children.append(e)

        xml = root.render(context={'val2': [7, 8, 9], 'val': 7}).serialize(output='xml')
        self.assertXmlEqual(xml, 'assets/elements/if_and_for.xml')

    def test_render_children(self):
        env = get_base_environment()
        template = env.get_template('template2.xml')

        template_root = template.root_element
        rendered_root = template_root.render({'iterable': [1, 2, 3], 'value': 8})
        self.assertEqual(rendered_root.node_name, 'root')

        self.assertEqual(len(template_root.children), 3)

        children = list(rendered_root.children)
        self.assertEqual(len(children), 4)

        self.assertEqual(children[0].node_name, 'child1')
        self.assertEqual(children[0].node_attributes, {'counter': 1})
        self.assertEqual(list(children[0].children), ['value1'])

        self.assertEqual(children[1].node_name, 'child1')
        self.assertEqual(children[1].node_attributes, {'counter': 2})
        self.assertEqual(list(children[1].children), ['value1'])

        self.assertEqual(children[2].node_name, 'child1')
        self.assertEqual(children[2].node_attributes, {'counter': 3})
        self.assertEqual(list(children[2].children), ['value1'])

        self.assertEqual(children[3].node_name, 'child3')
        self.assertEqual(children[3].node_attributes, {'attr1': 8})
        self.assertEqual(list(children[3].children), ['value3'])


class TestSerializing(SamonTestCase):
    def test_render_to_json(self):
        env = get_base_environment()
        template = env.get_template('template2.xml')

        rendered_root = template.root_element.render({'iterable': [1, 2, 3], 'value': 8})
        output = rendered_root.serialize(output='json')
        self.assertJsonEqual(output, 'assets/base/template2_rendered.json')

    def test_render_to_xml(self):
        env = get_base_environment()
        template = env.get_template('template2.xml')
        xml = template.root_element.render({'iterable': [1, 2, 3], 'value': 8}).serialize(output='xml')
        self.assertXmlEqual(xml, 'assets/base/template2_rendered.xml')

    def test_render_include(self):
        env = get_base_environment()
        template = env.get_template('template3.xml')
        xml = template.root_element.render({}).serialize(output='xml')
        self.assertXmlEqual(xml, 'assets/base/template3_rendered.xml')

    def test_element_custom_serializer(self):
        env = get_base_environment()

        @env.registry.element('child1')
        class Child1(BaseElement):
            def to_xml(self, io: StringIO, indent: int, rendered_element: RenderedElement):
                with rendered_element.frame(io, indent):
                    print(f'{constants.INDENT * (1 + indent)}<extra_tag>other content</extra_tag>', file=io)

        template = env.get_template('template4.xml')
        xml = template.root_element.render({}).serialize(output='xml')
        self.assertXmlEqual(xml, 'assets/base/template4_rendered.xml')
