from io import StringIO
from pathlib import Path
from unittest import TestCase

from samon import constants
from samon.elements import BaseElement, AnonymusElement
from samon.expressions import Condition, ForLoop, Bind


class BaseElementTest(TestCase):
    def assertXmlEqual(self, generated_xml: str, xml_benchmark: Path):
        xml_benchmark = Path(__file__).parent / xml_benchmark
        with xml_benchmark.open('r', encoding='utf-8') as f:
            xml_benchmark = f.read()

        self.assertEqual(generated_xml, xml_benchmark)

    def test_parse_xml_attributes(self):
        xml_attrs = {  # AttributesNSImpl like object
            (None, 'attr1'): 'val1',  # NS, attr_name
            ('http://example.org', 'attr2'): 'val2'
        }

        element = BaseElement(xml_tag='tag', xml_attrs=xml_attrs)
        self.assertEqual(element.xml_attrs, {'attr1': 'val1', '{http://example.org}attr2': 'val2'})

    def test_parse_expressions(self):
        xml_attrs = {
            (constants.XML_NAMESPACE_FLOW_CONTROL, 'if'): 'val == 7',
            (constants.XML_NAMESPACE_FLOW_CONTROL, 'for'): 'a in val',
            (constants.XML_NAMESPACE_DATA_BINDING, 'attr2'): 'val'
        }

        e = BaseElement(xml_tag='tag', xml_attrs=xml_attrs)
        self.assertIsInstance(e.xml_attrs['{https://doculabs.io/2020/xtmpl#control}if'], Condition)
        self.assertIsInstance(e.xml_attrs['{https://doculabs.io/2020/xtmpl#control}for'], ForLoop)
        self.assertIsInstance(e.xml_attrs['{https://doculabs.io/2020/xtmpl#data-binding}attr2'], Bind)
