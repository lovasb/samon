import json

from pathlib import Path
from unittest import TestCase


class SamonTestCase(TestCase):
    maxDiff = None

    def assertXmlEqual(self, generated_xml: str, xml_benchmark: Path):
        xml_benchmark = Path(__file__).parent / xml_benchmark
        with xml_benchmark.open('r', encoding='utf-8') as f:
            xml_benchmark = f.read()

        self.assertEqual(generated_xml, xml_benchmark)

    def assertJsonEqual(self, generated_json: list, json_benchmark: Path):
        json_benchmark = Path(__file__).parent / json_benchmark
        with json_benchmark.open('r', encoding='utf-8') as f:
            json_benchmark = json.load(f)

        self.assertSequenceEqual(generated_json, json_benchmark)
