from copy import copy
from unittest import TestCase

from samon.registry import registry
from samon.elements import BaseElement
from samon.exceptions import ElementNameConflict


class RegistryTest(TestCase):
    def test_empty_registry(self):
        """
        If registry haven't any key to the specific XML tag, then use the BaseElement
        """
        class Tag1:
            pass

        self.assertEqual(registry.get_class_by_name('Tag1'), BaseElement)
        registry.element(Tag1)
        self.assertEqual(registry.get_class_by_name('Tag1'), Tag1)

    def test_register_by_decorator(self):
        @registry.element('tag2')
        class Tag2:
            pass

        self.assertEqual(registry.get_class_by_name('tag2'), Tag2)

    def test_register_multiple(self):
        @registry.element('tag3')
        @registry.element('tag4')
        class Tag3:
            pass

        self.assertEqual(registry.get_class_by_name('tag3'), Tag3)
        self.assertEqual(registry.get_class_by_name('tag4'), Tag3)

    def test_register_name_collision(self):
        @registry.element('tag5')
        class Tag5:
            pass

        self.assertRaises(ElementNameConflict, lambda: registry.element('tag5', object.__class__))

    def test_register_with_namespace(self):
        @registry.element('tag6', namespace='https://example.org')
        class Tag6:
            pass

        self.assertEqual(registry.get_class_by_name('tag6'), BaseElement)
        self.assertEqual(registry.get_class_by_name('{https://example.org}tag6'), Tag6)

    def test_copy_registry(self):
        registry2 = copy(registry)

        self.assertEqual(registry2.xml_to_object_mapper, registry.xml_to_object_mapper)
        registry2.element('tag7', object.__class__)

        self.assertNotEqual(registry2.xml_to_object_mapper, registry.xml_to_object_mapper)
        self.assertNotIn('tag7', registry.xml_to_object_mapper.keys())
        self.assertIn('tag7', registry2.xml_to_object_mapper.keys())